package sks.sergey;

import javax.swing.*;
import java.awt.*;

/**
 * Created by SKS on 24.08.2017.
 */
public class Enemy {                                                   // машина соперника
    int x;
    int y;
    int v;

    Image img = new ImageIcon("src\\sks\\sergey\\res\\enemy.png").getImage();

    Panel panel;

    public Rectangle getRect(){                                             // столкновения
        return new Rectangle(x, y, 80, 180);
    }

    public Enemy (int x, int y, int v, Panel panel){
        this.x = x;
        this.y = y;
        this.v = v;
        this.panel = panel;
    }

    public void move(){
        y = y + panel.player.v - v;
    }
}