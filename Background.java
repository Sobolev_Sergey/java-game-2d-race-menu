package sks.sergey;

/**
 * Фон
 */

import javax.swing.*;
import java.awt.*;

public class Background {
    Image imgRoad = new ImageIcon("src\\sks\\sergey\\res\\road.jpg").getImage();
    Image imgFon = new ImageIcon("src\\sks\\sergey\\res\\fonmenu.jpg").getImage();
    Image imgFonSetting = new ImageIcon("src\\sks\\sergey\\res\\fonMenuSetting2.png").getImage();

    public void draw(Graphics2D g) {
        if (Panel.state.equals(Panel.STATES.MENU)) {
            g.drawImage(imgFon, 0, 0, null);
        }
        /*
        if (Panel.state.equals(Panel.STATES.PLAY)) {
            g.drawImage(imgRoad, 0, 0, null);
        }
        */
    }
}
