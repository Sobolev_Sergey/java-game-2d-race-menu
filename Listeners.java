package sks.sergey;

import java.awt.event.*;

public class Listeners implements MouseListener, KeyListener, MouseMotionListener {

    // проверка нажатой клавиши
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();                           // получить код нажатой клавиши
        if (key ==Panel.c_menu.button_up.ch_code ) {
            Player.dv = 1;
        }
        if (key == Panel.c_menu.button_d.ch_code) {
            Player.dv = -1;
        }
        if (key == Panel.c_menu.button_l.ch_code) {
            Player.dx = 4;
            Player.img = Player.img_l;
        }
        if (key == Panel.c_menu.button_r.ch_code) {
            Player.dx = -4;
            Player.img = Player.img_r;
        }
        if (key == KeyEvent.VK_ESCAPE) {
            if (Panel.state == Panel.STATES.PLAY)Panel.state = Panel.STATES.MENU; // переход в меню из игры
        }
        if( Panel.c_menu.button_up.replace){
            Panel.c_menu.button_up.ch_code = e.getKeyCode();
        }
        if( Panel.c_menu.button_d.replace){
            Panel.c_menu.button_d.ch_code = e.getKeyCode();
        }
        if( Panel.c_menu.button_l.replace){
            Panel.c_menu.button_l.ch_code = e.getKeyCode();
        }
        if( Panel.c_menu.button_r.replace){
            Panel.c_menu.button_r.ch_code = e.getKeyCode();
        }
        /*
        if( Panel.c_menu.button_f.replace){
            Panel.c_menu.button_f.ch_code = e.getKeyCode();
        }
        */
    }
    // проверка отжатой клавиши
    public void keyReleased(KeyEvent e){
        int key = e.getKeyCode();
        if (key == Panel.c_menu.button_up.ch_code) {
            Player.dv = 0;
        }
        if (key == Panel.c_menu.button_d.ch_code) {
            Player.dv = 0;
        }
        if (key == Panel.c_menu.button_l.ch_code) {
            Player.dx = 0;
            Player.img = Player.img_c;                          // отпускаем клавишу - машина опять едет прямо
        }
        if (key == Panel.c_menu.button_r.ch_code) {
            Player.dx = 0;
            Player.img = Player.img_c;                          // отпускаем клавишу - машина опять едет прямо
        }
    }

    public void keyTyped(KeyEvent e){
        char ch = e.getKeyChar();
        if( Panel.c_menu.button_up.replace){                    // присвоим  надписи символ как строка
            Panel.c_menu.button_up.f=  String.valueOf(ch);
            Panel.c_menu.button_up.replace = false;
        }
        if( Panel.c_menu.button_d.replace){
            Panel.c_menu.button_d.f= String.valueOf(e.getKeyChar());
            Panel.c_menu.button_d.replace= false;
        }
        if( Panel.c_menu.button_l.replace){
            Panel.c_menu.button_l.f= String.valueOf(e.getKeyChar());
            Panel.c_menu.button_l.replace = false;
        }
        if( Panel.c_menu.button_r.replace){
            Panel.c_menu.button_r.f= String.valueOf(e.getKeyChar());
            Panel.c_menu.button_r.replace=false;
        }
        /*
        if( Panel.c_menu.button_f.replace){
            Panel.c_menu.button_f.f= String.valueOf(e.getKeyChar());
            Panel.c_menu.button_f.replace=false;
        }
        */
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (Panel.state == Panel.STATES.MENU) {
                Menu.click = true;                              // нажатие ЛКМ в меню
            }
        }
    }

    public void mouseReleased(MouseEvent e){
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (Panel.state == Panel.STATES.MENU) {
                Menu.click = false;                             // отпуск ЛКМ в меню
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {                    // метод переноса мышкой
       
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Panel.mouseX = e.getX();                                // получить координату х при перемещении мышки
        Panel.mouseY = e.getY();                                // получить координату у при перемещении мышки
    }
}
