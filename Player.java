package sks.sergey;

import javax.swing.*;
import java.awt.*;

public class Player {

    public static final int MAX_V = 70;                             // макс скорость пикс за обновление
    public static final int MAX_LEFT = 240;                         // макс координата нашей машины отн лев
    public static final int MAX_RIGHT = 700;                        // макс коорд отн прав

    // стаич прем - кавиши перемещения
    public static boolean up = false;
    public static boolean down = false;
    public static boolean left = false;
    public static boolean right = false;

    public static String img_c = "src\\sks\\sergey\\res\\player.png";
    public static String img_l = "src\\sks\\sergey\\res\\playerL.png";
    public static String img_r = "src\\sks\\sergey\\res\\playerR.png";
    public static String img = img_c;

    public static int dx = 0;
    public static int dv = 0;                                       // ускорение
    int s = 0;                                                      // пройденный путь
    int v = 10;                                                     // начальная скорость машины
    int x;
    int y;

    // дорога
    int layer1 = 0;                                                 // координата первого слоя
    int layer2 = 1554;                                              // координата второго слоя

    public Player(){
        x = 512;                                                    // координаты появления
        y = 550;
    }

    public void move() {                                            // метод езды
        s += v;                                                     // накапливаем путь
        v += dv;

        if (v <= 0) v = 0;
        if (v >= MAX_V) v = MAX_V;

        x -= dx;
        if (x <= MAX_LEFT) x = MAX_LEFT;
        if (x >= MAX_RIGHT) x = MAX_RIGHT;

        if (layer2 - v <= 0){                                       // зацикливаем прорисовку дороги
            layer1 = 0;
            layer2 = 1554;
        } else {
            layer1 -= v;                                            // а координаты уменьшились
            layer2 -= v;
        }
    }

    // обновления
    public void update(){
        //if(Panel.easy) v = 10;
        //if(Panel.norm) v = 20;
        //if(Panel.hard) v = 30;

        // смещение машины по дороге
        if (up){
            dv = 1;
        }
        if (down){
            dv = -1;
        }
        if (left){
            dx = 4;
            img = img_l;                                            // поворот на лево
        }
        if (right){
            dx = -4;
            img = img_r;
        }
    }

    // столкновения
    public Rectangle getRect(){
        return new Rectangle(x, y, 100, 180);
    }

    // отрисовка машины
    public void draw(Graphics2D g){
        g.drawImage(new ImageIcon(img).getImage(),(int)x,(int)y,null);
    }
}
