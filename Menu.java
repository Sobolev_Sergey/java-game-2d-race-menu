package sks.sergey;

import javax.swing.*;
import java.awt.*;

public class Menu {

    private String imgbut = "src\\sks\\sergey\\res\\but.png";
    private String imgButHover = "src\\sks\\sergey\\res\\buthover.png";
    public static boolean click = false;
    public boolean user_m = false;
    public boolean game_m = false;

    //  объявление кнопок
    ButtonMenu button1 = new ButtonMenu(300,10,305,120,"","");
    ButtonMenu button2 = new ButtonMenu(300,150,305,120,"","");
    ButtonMenu button3 = new ButtonMenu(300,300,305,120,"","");
    ButtonMenu button4 = new ButtonMenu(300,450,305,120,"","");
    ButtonMenu button5 = new ButtonMenu(300,600,305,120,"","");

   // вызов метода для прорисовки кнопки
    public void draw(Graphics2D g) {
        button1.draw(g);
        button2.draw(g);
        button3.draw(g);
        button4.draw(g);
        button5.draw(g);
    }

    // попадание курсора на кнопку меню
    public void moveButt(ButtonMenu e){
        if (Panel.mouseX >e.getX() && Panel.mouseX <e.getX() + e.getW()  &&
                Panel.mouseY >e.getY() && Panel.mouseY < e.getY()+ e.getH())
        {// если курсор попал в кнопку
            e.s = imgButHover;

            if(e.equals(button1)) {
                e.f="new user";
                user_m = true;                                 // курсор на кнопке
            }
            if(e.equals(button2)) {
                e.f="game";
                game_m = true;
                if (Menu.click) {                              //клик ЛКМ
                    Panel.state = Panel.STATES.PLAY;           //переход в игру
                    Menu.click = false;
                }
            }
            if(e.equals(button3)) {
                e.f="settings";
                if(Menu.click){
                    Panel.settmenu = true;
                    Panel.buttmenu = false;
                }
            }
            if(e.equals(button4)) {
                e.f="specification";
            }
            if(e.equals(button5)) {
                e.f="exit";
                if(Menu.click){
                    System.exit(0);
                }
            }
        }
        else{
            e.s = imgbut;
            if(e.equals(button1)) {
                e.f="новый игрок";
                user_m = false;                                 // курсор не на кнопке
            }
            if(e.equals(button2)) {
                e.f="играть";
                game_m = false;
            }
            if(e.equals(button3)) {
                e.f="настройки";
            }
            if(e.equals(button4)) {
                e.f="правила";
            }
            if(e.equals(button5)) {
                e.f="выход";
            }
        }
    }

    class ButtonMenu {
        //  нач координаты и размер объекта
        private double x;                                           // координа х обьекта
        private double y;                                           // координа y
        private double w;                                           // ширина обьекта
        private double h;                                           // высота объекта

        public Color colorText;                                     // цвет
        public String f;                                            // надпись над кнопкой
        public String s;                                            // строка адреса картинки

        public ButtonMenu(int x, int y, int w, int h, String s, String f){
            this.x = x;                                             // нач координаты героя
            this.y = y;
            this.w = w;
            this.h = h;
            this.f = f;
            this.s = s;
            colorText = Color.WHITE;
        }

        public double getX(){ return  this.x; }
        public double getY(){ return  this.y; }
        public double getW(){ return  this.w; }
        public double getH(){ return  this.h; }

        // отрисовка объекта
        public void draw(Graphics2D g){
            g.drawImage( new ImageIcon(s).getImage(), (int)x,(int)y, null);
            g.setColor(colorText);
            Font font = new Font("Arial",Font.ITALIC,40);
            g.setFont(font);
            long length = (int) g.getFontMetrics().getStringBounds(f,g).getWidth();
            g.drawString(f,(int)(x+w/2) - (int)(length / 2),(int)y+(int)(h/ 3)*2-20);

            if (user_m){
                font = new Font("Arial", Font.ITALIC, 36);
                g.setFont(font);
                g.drawString("Введите ваше имя", 650, 100);
                g.drawString("Player1", 150, 100);
            }
            if (game_m) {

                g.drawImage(new ImageIcon("src\\sks\\sergey\\res\\playerMenu.png").getImage(),
                        50, 175, null);
                font = new Font("Arial", Font.ITALIC, 40);
                g.setFont(font);
                g.drawString("Начать игру", 650, 230);
            }
        }
    }
}