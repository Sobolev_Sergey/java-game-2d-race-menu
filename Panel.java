package sks.sergey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.List;

/**
 Панель игры
 */

public class Panel  extends JPanel implements ActionListener, Runnable {

    //размер панели
    public static int WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
    public static int HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;

    // координаты мышки
    public static int mouseX;
    public static int mouseY;

    // активные страницы меню
    public static boolean buttmenu = true;                          // главная страница меню
    public static boolean settmenu = false;                         // страница меню настроек
    public static boolean controlmenu = false;                      // страница меню настроек

    public static boolean easy = true;                              // уровень сложности легко
    public static boolean norm = false;                             // уровень сложности средне
    public static boolean hard = false;                             // уровень сложности сложно
    public static boolean aud = true ;
    public static boolean control = true;                           // управление по умолчанию

    public static MenuControl c_menu;

    public enum STATES{
        MENU,PLAY
    }
    public static STATES state = STATES.MENU;                       // изначально - меню

    private BufferedImage image;
    private Graphics2D g;


    public ArrayList<MenuSetting> buttons;                          // список кнопок
    Timer mainTimer = new Timer(30,this);             // задает интервал обновления всех событий

    Background background = new Background();
    Player player = new Player();
    Menu menu = new Menu();

    Thread enemiesFactory = new Thread(this);                // в новом потоке будут враги
    List<Enemy> enemies = new ArrayList<Enemy>();                       // лист наших врагов

    Image img = new ImageIcon("src\\sks\\sergey\\res\\road.jpg").getImage();
    Image imgFonSetting = new ImageIcon("src\\sks\\sergey\\res\\fonMenuSetting2.png").getImage();

    public Panel() {
        super();
        setFocusable(true);                                         // передаем фокус
        requestFocus();                                             // акивируем
        mainTimer.start();
        enemiesFactory.start();                                     // запуск потока врагов
        c_menu = new MenuControl();

        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        g = (Graphics2D) image.getGraphics();

        buttons = new ArrayList<>();
        buttons.add(new MenuSetting(400,130,200,37,"","легко"));
        buttons.add(new MenuSetting(400,180,200,37,"","средне"));
        buttons.add(new MenuSetting(400,230,200,37,"","сложно"));

        buttons.add(new MenuSetting(400,360,200,37,"","вкл"));
        buttons.add(new MenuSetting(400,420,200,37,"","выкл"));

        buttons.add(new MenuSetting(400,550,200,37,"","стандарт"));
        buttons.add(new MenuSetting(400,600,200,37,"","пользоват"));

        buttons.add(new MenuSetting(850,700,150,37,"","назад"));

        addMouseListener( new Listeners());                          // обработчик событий клик мыши
        addKeyListener( new Listeners());                            // обработчик событий клавиатура
        addMouseMotionListener(new Listeners());                     // обработчик событий перемещение мыши
    }

    public void actionPerformed(ActionEvent e){                      // все события игры
        // ЕСЛИ МЕНЮ
        if (state.equals(STATES.MENU)){
            background.draw(g);                                      // отобразить фон

            // главная страница меню
            if(buttmenu){
                menu.draw(g);                                        // отобразить меню
                menu.moveButt(menu.button1);
                menu.moveButt(menu.button2);
                menu.moveButt(menu.button3);
                menu.moveButt(menu.button4);
                menu.moveButt(menu.button5);
            }

            // страница меню настроек
            if(settmenu){
                g.drawImage(imgFonSetting, 0, 0, null);
                moveSettButt();
            }

            // страница изменений клавиш управления
            if(controlmenu){
                g.drawImage(imgFonSetting, 0, 0, null);
                c_menu.draw(g);
                c_menu.moveContr(c_menu.button_up);
                c_menu.moveContr(c_menu.button_d);
                c_menu.moveContr(c_menu.button_l);
                c_menu.moveContr(c_menu.button_r);
                //c_menu.moveContr(c_menu.button_f);
                c_menu.moveContr(c_menu.button_k);
            }
            gameDraw();                                                 // прорисовать в панели
        }

        // ЕСЛИ ИГРА
        if (state.equals(STATES.PLAY)){
            player.update();
            player.draw(g);
            player.move();
            gameDraw();
            testCollisionWithEnemies();
            testWin();
        }
    }

    public void moveSettButt(){
        for(int i = 0; i < buttons.size(); i++ ){
            buttons.get(i).draw(g);
            if (Panel.mouseX >buttons.get(i).getX() && Panel.mouseX <buttons.get(i).getX() +buttons.get(i).getW()  &&
                    Panel.mouseY >buttons.get(i).getY() && Panel.mouseY <buttons.get(i).getY()+ buttons.get(i).getH()){
                //buttons.get(i).s = "image/but3.png";
                if (i==0) {
                    if(Menu.click){
                        easy=true;
                        norm=false;
                        hard=false;
                    }
                }
                if (i==1) {
                    if(Menu.click){
                        norm=true;
                        easy=false;
                        hard=false;
                    }
                }
                if (i==2) {
                    if(Menu.click){
                        hard=true;
                        norm=false;
                        easy=false;
                    }
                }
                if (i==3) {
                    if(Menu.click){
                    aud=true;
                    }
                }
                if (i==4) {
                    if(Menu.click){
                    aud=false;
                    }
                }
                if (i==5) {
                    if(Menu.click){                                     // клавиша стандарт управление
                    control=true;
                    c_menu.button_up.f= "стрелка вверх";
                    c_menu.button_d.f= "стрелка вниз";
                    c_menu.button_l.f= "стрелка влево";
                    c_menu.button_r.f= "стрелка вправо";
                    //c_menu.button_f.f= "Space";
                    // передача кодов клавиш стандартного управления
                    c_menu.button_up.ch_code=38;
                    c_menu.button_d.ch_code=40;
                    c_menu.button_l.ch_code=37;
                    c_menu.button_r.ch_code=39;
                    //c_menu.button_f.ch_code=32;
                    }
                }
                if (i==6) {
                    if(Menu.click){
                    control=false;
                    settmenu=false;
                    controlmenu = true;
                    }
                }
                if (i==7) {
                    if(Menu.click){
                        settmenu=false;
                        buttmenu=true;

                    }
                }
            }
            else{

                //buttons.get(i).s = "image/but4.png";
            }
        }
    }

    //перенос изображения на панель
    private void gameDraw() {

        Graphics g2 = this.getGraphics();                               // переоппред Graphics2d на Graphics
        g2.drawImage(image, 0, 0, null);                 // рисуем
        g2.dispose();                                                   // команда для уборщщика мусора

        g = (Graphics2D) g;                                             // приведение типа
        g.drawImage(img, 0, -player.layer1, null);          // рисуем дорогу 1й слой
        g.drawImage(img, 0, -player.layer2, null);          // рисуем дорогу 2й слой

        double v = (200/Player.MAX_V) * player.v;
        double s = player.s/100;
        double sFinish = (200000 - player.s)/100;
        g.setColor(Color.WHITE);
        Font font = new Font("Arial", Font.BOLD, 20);
        g.setFont(font);
        g.drawString("Скорость: " + v + "   км/ч"
                + "      пройденный путь: " + s + " м"
                + "      до финиша осталось: " + sFinish + " м", 50, 30);

        Iterator<Enemy> i = enemies.iterator();                         // пробегаемся по элементам
        while (i.hasNext()){                                            // пока существует след элемент
            Enemy e = i.next();                                         // получаем новый объект
            if (e.y >=1200 || e.y <= -500) {                            // если враг далеко за пределами
                i.remove();                                             // удаляем его
            } else {
                e.move();
                g.drawImage(e.img, e.x, e.y, null);            // иначе рисуем врага
            }
        }
    }

    private void testWin() {
        if (player.s > 200000){
            JOptionPane.showMessageDialog(null, "Победа!!!");
            System.exit(1);
        }
    }

    private void testCollisionWithEnemies() {                           // метод проверки столкновения
        Iterator<Enemy> i = enemies.iterator();
        while (i.hasNext()){
            Enemy e = i.next();
            if (player.getRect().intersects(e.getRect())){               // если прямоугольники столкнулись
                i.remove();                                              // удаляем
                JOptionPane.showMessageDialog(null, "Авария!!!");
                System.exit(1);
            }
        }
    }

    @Override
    public void run() {                                                 // точка входа в наш 2й поток
        while (true){
            Random rand = new Random();
            try {
                Thread.sleep(rand.nextInt(1000));               // усыпим поток случайно от 0 до 2000
                Iterator<Enemy> i = enemies.iterator();                // пробегаемся по элементам
                while (!i.hasNext()){                                  // если врага не существует -> создаем его
                    enemies.add(new Enemy(  rand.nextInt( 560)+240,-300,
                            rand.nextInt(80),this));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
