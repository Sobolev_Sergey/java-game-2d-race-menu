package sks.sergey;

import javax.swing.*;
import java.awt.*;

/**
 Подменю настройки
 */

public class MenuSetting {

    private String imgSoundOn = "src\\sks\\sergey\\res\\soundOn.png";
    private String imgSoundOff = "src\\sks\\sergey\\res\\soundOff.png";

    // нач координаты и размер объекта
    private double x;                                           // координа х обьекта
    private double y;                                           // координа y
    private double w;                                           // ширина обьекта
    private double h;                                           // высота объекта

    public Color colorText;                                     // цвет надписи
    public String f;                                            // надпись над кнопкой
    public String s;                                            // строка адреса картинки

    public MenuSetting(int x, int y, int w, int h, String s, String f){
        this.x = x;                                             // нач координаты героя
        this.y = y;
        this.w = w;
        this.h = h;
        this.f = f;
        this.s = s;
        colorText = Color.WHITE;
    }

    public double getX(){ return  this.x; }
    public double getY(){ return  this.y; }
    public double getW(){ return  this.w; }
    public double getH(){ return  this.h; }

    // отрисовка объекта
    public void draw(Graphics2D g){
        g.setColor(colorText);
        Font font = new Font("Arial", Font.ITALIC, 50);
        g.setFont(font);
        g.drawString("Н А С Т Р О Й К И", 300, 70);

        g.setColor(Color.red);
        font = new Font("Arial",Font.ITALIC,50);
        g.setFont(font);
        g.drawString("Сложность", 40,200);
        g.drawString("Звук", 40,400);
        g.drawString("Управление", 40,600);

        g.setColor(colorText);
        Font font1 = new Font("Arial",Font.ITALIC,40);
        g.setFont(font1);

        long length = (int) g.getFontMetrics().getStringBounds(f,g).getWidth();
        g.drawString(f,(int)(x+w/2) - (int)(length / 2),(int)y+(int)(h/ 3)*2);

        g.setColor(Color.red);
        Font font2 = new Font("Arial",Font.ITALIC,40);
        g.setFont(font2);

        if(Panel.easy)g.drawString("легко", 800,200);
        if(Panel.norm)g.drawString("средне", 800,200);
        if(Panel.hard)g.drawString("сложно", 800,200);

        if(Panel.aud)g.drawImage( new ImageIcon(imgSoundOn).getImage(), 800,350, null);
        if(!Panel.aud)g.drawImage( new ImageIcon(imgSoundOff).getImage(), 800,350, null);

        if(Panel.control)g.drawString("стандарт", 700,600);
        if(!Panel.control)g.drawString("пользовательское", 630,600);
    }
}



