package sks.sergey;

import javax.swing.*;
import java.awt.*;

/**
 Окно меню для изменения кнопок управления
 */
public class MenuControl {
    private String imgBut = "src\\sks\\sergey\\res\\butcontrolhover.png";
    private String imgButHover = "src\\sks\\sergey\\res\\butcontrol.png";

    // объявление кнопок
    ButtСontrol button_up = new ButtСontrol(100,200,360,88,"","стрелка вверх",38);
    ButtСontrol button_d = new ButtСontrol(100,300,360,88,"","стрелка вниз",40);
    ButtСontrol button_l = new ButtСontrol(100,400,360,88,"","стрелка влево",37);
    ButtСontrol button_r = new ButtСontrol(100,500,360,88,"","стрелка вправо",39);
    //ButtСontrol button_f = new ButtСontrol(550,410,100,37,"image/but4.png","Space",32);
    ButtСontrol button_k = new ButtСontrol(600,630,360,88,"","назад",8);

    // вызов метода для прорисовки кнопки
    public void draw(Graphics2D g) {
        button_up.draw(g);
        button_d.draw(g);
        button_l.draw(g);
        button_r.draw(g);
        //button_f.draw(g);
        button_k.draw(g);
    }
    // попадание курсора на кнопку меню
    public void moveContr(ButtСontrol b) {
        if (Panel.mouseX > b.getX() && Panel.mouseX < b.getX() + b.getW() &&
                Panel.mouseY > b.getY() && Panel.mouseY < b.getY() + b.getH()) {
            b.s = imgBut;
            if (b.equals(button_up)) {
                    editContr(b);
            }
            if (b.equals(button_d)) {
                    editContr(b);
            }
            if (b.equals(button_l)) {
                    editContr(b);
            }
            if (b.equals(button_r)) {
                    editContr(b);
            }
            if (b.equals(button_k)) {
                if (Menu.click) {
                    Panel.buttmenu = false;
                    Panel.controlmenu = false;
                    Panel.settmenu = true;
                }
            }
        }else {
                b.s = imgButHover;
                b.replace = false;
        }
    }

    // попадание курсора на кнопку меню
    private void editContr(ButtСontrol b){
        if (Menu.click){
        b.f = "";
        b.replace = true;
        }
    }

    class ButtСontrol{
        //  нач координаты и размер объекта
        private double x;
        private double y;
        private double w;
        private double h;

        public Color color1;

        public String f;
        public int ch_code;
        public String s;
        public boolean replace=false;

        public ButtСontrol(int x,int y,int w,int h,String s,String f,int ch_code){
            this.x =x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.f = f;
            this.s = s;
            this.ch_code = ch_code;
            color1 = Color.WHITE;
            this.replace = replace;

        }

        public double getX(){ return  this.x; }
        public double getY(){ return  this.y; }
        public double getW(){ return  this.w; }
        public double getH(){ return  this.h; }

        // отрисовка объекта
        public void draw(Graphics2D g) {
            g.drawImage(new ImageIcon(s).getImage(), (int) x, (int) y, null);
            g.setColor(color1);
            Font font = new Font("Arial", Font.ITALIC, 40);
            g.setFont(font);
            g.drawString("КЛАВИШИ УПРАВЛЕНИЯ", 300, 100);

            font = new Font("Arial", Font.ITALIC, 30);
            g.setFont(font);
            long length = (int) g.getFontMetrics().getStringBounds(f, g).getWidth();
            g.drawString(f, (int) (x + w / 2) - (int) (length / 2), (int) y + (int) (h / 3) * 2);

            g.setColor(Color.red);
            Font font1 = new Font("Arial", Font.ITALIC, 40);
            g.setFont(font1);

            g.drawString("ускорение", 600, 250);
            g.drawString("замедление", 600, 350);
            g.drawString("влево", 600, 450);
            g.drawString("вправо", 600, 550);

        }
    }
}